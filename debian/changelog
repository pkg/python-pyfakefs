python-pyfakefs (4.6.3-3+apertis0) apertis; urgency=medium

  * Sync from debian/bookworm.

 -- Apertis CI <devel@lists.apertis.org>  Fri, 07 Apr 2023 23:13:27 +0000

python-pyfakefs (4.6.3-3) unstable; urgency=medium

  * Team upload.
  * add pytest to B-D to force tests to run via something other
    than the deprecated "python3 setup.py test" command

 -- Mohammed Bilal <mdbilal@disroot.org>  Sun, 04 Dec 2022 08:09:59 +0000

python-pyfakefs (4.6.3-2) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Repository-Browse.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 20:56:49 +0100

python-pyfakefs (4.6.3-1) unstable; urgency=medium

  [ Ileana Dumitrescu ]
  * Team upload.
  * New upstream release, fixing FTBFS (Closes: #1017210)
  * d/copyright: Update debian copyright year to 2022.
  * d/control: Bump standards version to 4.6.1.

  [ Jeroen Ploemen ]
  * Copyright: bump upstream copyright year.

 -- Ileana Dumitrescu <ileanadumitrescu95@gmail.com>  Mon, 12 Sep 2022 20:03:40 +0000

python-pyfakefs (4.5.4-1) unstable; urgency=medium

  * Team upload

  [ Alexandre Ghiti ]
  * New upstream release, fixing FTBFS (Closes: #1002350)
  * d/patches: Remove disable_test.patch that is now useless.

 -- Graham Inggs <ginggs@debian.org>  Fri, 11 Feb 2022 15:42:25 +0000

python-pyfakefs (4.3.3-1apertis0) apertis; urgency=medium

  * Import from Debian bullseye.
  * Set component to development.

 -- Apertis package maintainers <packagers@lists.apertis.org>  Wed, 21 Apr 2021 11:17:21 +0200

python-pyfakefs (4.3.3-1) unstable; urgency=medium

  * Add d/gbp.conf.
  * New upstream release.
  * Disable not working test.
  * Bump standards version to 4.5.1.
  * d/watch: Bump version.

 -- Ondřej Nový <onovy@debian.org>  Mon, 04 Jan 2021 12:43:44 +0100

python-pyfakefs (4.1.0-1) unstable; urgency=medium

  [ Ondřej Nový ]
  * New upstream release.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.
  * Drop d/p/remove_flask_restx.patch: Removed, not needed.

  [ Debian Janitor ]
  * Remove obsolete field Name from debian/upstream/metadata (already present in
    machine-readable debian/copyright).

 -- Ondřej Nový <onovy@debian.org>  Tue, 27 Oct 2020 10:43:48 +0100

python-pyfakefs (4.0.2-1) unstable; urgency=medium

  * New upstream release.
  * Drop upstream depends on (not used) flask_restx.

 -- Ondřej Nový <onovy@debian.org>  Fri, 12 Jun 2020 13:54:38 +0200

python-pyfakefs (3.7.2-2) unstable; urgency=medium

  * Drop PyPy support.
  * Bump debhelper compat level to 13.

 -- Ondřej Nový <onovy@debian.org>  Fri, 05 Jun 2020 15:54:05 +0200

python-pyfakefs (3.7.2-1) unstable; urgency=medium

  * New upstream release.

 -- Ondřej Nový <onovy@debian.org>  Sat, 07 Mar 2020 20:58:42 +0100

python-pyfakefs (3.7.1-1) unstable; urgency=medium

  * Initial release. (Closes: #950617)

 -- Ondřej Nový <onovy@debian.org>  Tue, 04 Feb 2020 10:17:24 +0100
